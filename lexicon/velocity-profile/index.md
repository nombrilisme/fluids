---
time: 202304041743
title: "velocity profile"
type: lexicon
domain: fluids

what:
  0: |
    the {{< 词典 flow velocity >}} evaluated along a line.
---

