---
title: "shear stress"
type: lexicon
colour: greek
domain: fluids

what:
  0: |
    the component of {{< 词典 stress >}} that's coplanar with the body's cross 
    section.
    ```
      τ = ÷FA
    ```
    where ˝F˝ is the {{< 词典 shear force >}} and ˝A˝ is the cross sectional 
    area of the material that's parallel to the force. (note that "planar" means 
    there's a plane that contains all the points).

    {{< figure shearstress.png 10 >}}

    contrast with "ordinary" {{< 词典 stress >}}, which comes from the force 
    vector component perpendicular to the material cross section.
---
