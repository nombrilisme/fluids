---
time: 202304041733
title: "incompressible fluid"
type: lexicon
domain: fluids

what:
  0: |
    a fluid where
    ££
      ∇⦁⬀v = 0
    ££
    note that calling a fluid "incompressible" is very much an idealisation.  

    the incompressible approximation is valid when the characteristic velocities 
    of the flow are much less than the speed of sound in the fluid (since then 
    you don't have to worry about density variations)

  1: |
    a fluid where ρ = const. 

    this is because to compress the fluid, you have to induce some kind of 
    density variation. a fluid with constant (or, more realistically, 
    _approximately_ constant) density is incompressible.

  2: |
    a fluid where variations of pressure are so small that changes in density 
    are negligible.

---
