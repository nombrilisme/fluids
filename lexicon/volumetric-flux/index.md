---
title: "volumetric flux"
time: 202305161504
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    the rate of volume flow, or volume of fluid that passes per unit time.

    {{< figure DarcysLaw.jpg >}}
---
