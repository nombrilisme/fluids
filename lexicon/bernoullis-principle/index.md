---
time: 20230418171
title: "bernoulli's principle"
type: lexicon
domain: fluids

what:
  0: |
    increasing the speed of a fluid decreases the static pressure or potential 
    energy. 

    only applies for isentropic flows (no irreversible processes like turbulence 
    or thermal radiation). so basically, we're talking about fluids and gases at 
    low mach number. the incompressible approximation applies.

  1: |
    bernoulli's equation:
    ££
      ½v² + gz + ⌠1⧸ρ⌡p = const.
    ££
    applies in any region of a flow where energy per unit mass is uniform.

    this accurately describes fluid flow only in regions where viscous forces 
    don't exist (and diminish the energy per unit mass).

  2: |
    another common statement of bernoulli's principle:
    ££
      ½v² + ∫dPͺ⌠1⧸ρ⌡ = const
    ££

    assumptions:
    - steady flow (parameters like ˝v, p, ρ˝ are constant in time)
    - incompressible
    - no friction by viscous forces

---
