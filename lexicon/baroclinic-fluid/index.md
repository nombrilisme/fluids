---
title: "baroclinic fluid"
time: 202305161516
type: lexicon
domain: fluids

aka: 

alsoread: 
- https://en.wikipedia.org/wiki/Baroclinity

what:
  0: |
    a fluid where the density ˝ρ˝ depends on both pressure ˝p˝ _and_ temperature 
    ˝T˝.

    is the more "general" case of a {{< 词典 barotropic fluid >}}.
---
