---
title: "circulation"
time: 202305151332
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    given a surface ˝S˝, the circulation ˝Γ˝ is the line integral of the velocity 
    field around the boundary of the surface ˝𝝏S˝:
    ££
      Γ = ∫₍𝝏S₎ ⬀v ⦁ d⬀x
    ££
    which by stokes' theorem is also equal to the flux of vorticity across the 
    surface:
    ££
      Γ = ∫₍S₎ ⬀ω ⦁ d⬀A
    ££
---
