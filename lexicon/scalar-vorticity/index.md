---
title: "scalar vorticity"
time: 202305162204
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    in a purely 2d flow the {{< 词典 vorticity >}} will point exclusively in the 
    z-direction so you can think of it as a scalar quantity (it's characterised 
    by its magnitude not its direction). in this case you can take only the 
    z-component of the cross product and express the vorticity as
    ££
      ω = (𝝏₍x₎v₍y₎ - 𝝏₍y₎v₍x₎)⇧e₍z₎
    ££
---
