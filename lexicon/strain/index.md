---
time: 202304041703
title: "strain"
type: lexicon
domain: fluids

what:
  0: |
    a measure of deformation of an object. it describes the displacement between 
    particles relative to a reference length.


    strains are usually dimensionless

    whenever a solid material undergoas a strain (deformation), there is an 
    internal elastic stress that causes the object to restore to its original 
    non-deformed state (like the spring force)

    in liquids and gases, ONLY deformations that change the volume will generate a 
    persistent elastic stress
    - if the deformation changes gradually, in fluids there will be some viscous 
      stress that opposes the change.

    elastic and viscous stresses are both kinds of mechanical stress.

  1: |
    functional form of deformation:
    ```
      ↗x = F(↗x')
    ```
    - ↗x' is an array of reference positions for points of the body
    - deformations have units of length

---


