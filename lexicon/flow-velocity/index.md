---
title: "flow velocity"
time: 202305120507
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    the velocity field of the fluid, a function of space and time ˝⬀v(⬀x,t)˝.
---
