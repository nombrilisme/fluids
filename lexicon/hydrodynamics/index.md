---
time: 202304041734
title: "hydrodynamics"
type: lexicon
domain: fluids

what:
  0: |
    the study of fluids in motion, i.e. when the {{< 词典 flow velocity >}} 
    v(x,t) ≠ 0.

---
