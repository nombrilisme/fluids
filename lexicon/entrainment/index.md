---
time: 202305020908
title: "entrainment"
type: lexicon
domain: fluids

what:
  0: |
    the process where a fluid wake tends to "capture" fluid from outside the 
    wake, thereby increasing the width of the (turbulent) wake.

src:
  - TB p806
  - https://en.wikipedia.org/wiki/Coand%C4%83_effect
  - https://inv.riverside.rocks/watch?v=AvLwqRCbGKY
---
