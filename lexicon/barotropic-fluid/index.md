---
time: 202304041717
title: "barotropic fluid"
type: lexicon
domain: fluids

what:
  0: |
    a fluid where ρ is a function of p only.

    contrast with {{< 词典 baroclinic fluid >}}.
---

> so what's the difference between a barotropic fluid and a perfect fluid?
{.question}
