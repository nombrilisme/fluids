---
title: "mean free path"
time: 202305181034
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    the average distance a particle travels before changing its direction due to 
    collision/intereference with another particle.
---
