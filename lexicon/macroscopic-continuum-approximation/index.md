---
title: "macroscopic continuum approximation"
time: 202305181029
type: lexicon
domain: fluids

aka: 
- continuum hypothesis

linked: 

sources:
- TB
- TONG

what:
  0: |
    given a large, many-particle system (like a {{< 词典 fluid >}}), 

    IF the {{< 词典 mean free path >}} of particles is much smaller than typical 
    {{< 词典 macroscopic length scales >}}, 

    THEN you can approximate the system as a continuum, and describe the 
    configuration of the system using smooth continuous fields. these continuous 
    fields describe the _mean local_ value of a quantity at each point (in the 
    embedding domain).

    e.g. in fluid dynamics we use the continuum approximation: we assume
    the fluid is (approximately) an indivisible continuous object , and we 
    describe its configuration using the continuous fields ˝⬀v(⬀x,t)˝, 
    ˝ρ(⬀x,t)˝, and ˝p(⬀x,t)˝ {{< ℭ TONG ch1.1 >}}.

---
