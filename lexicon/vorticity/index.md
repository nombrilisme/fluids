---
time: 20230418171
title: "vorticity"
type: lexicon
domain: fluids

what:
  0: |
    the curl of the velocity field:

    ££ ⬀ω = ∇ × ⬀v ££

---
