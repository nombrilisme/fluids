---
title: "porous medium equation"
time: 202305161439
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a nonlinear pde of form:
    ```
      𝝏₍t₎u = κ Δ₍d₎(u⁽1+n⁾)
    ```
    where
    - ˝u = u(r,t)˝
    - ˝r = r(x₍1₎, x₍2₎, ..., x₍d₎) ∈ R⁽d⁾˝
    - ˝Δ₍d₎ = ∑{i=1..d} 𝝏₍x₍i₎₎²˝ (the {{< 词典 laplace operator >}})
---
