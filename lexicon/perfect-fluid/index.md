---
title: "perfect fluid"
time: 202302141032
type: lexicon
domain: fluids

aka:
- ideal fluid

alsoread:
- https://en.wikipedia.org/wiki/Perfect_fluid

what:
  0: |
    a fluid that you can fully describe using only its density ρ and pressure p.

  1: |
    a fluid that has no (or negligible) viscosity or shear stresses.

  2: |
    a fluid where viscosity is negligible and heat conductivity is negligible.
---
