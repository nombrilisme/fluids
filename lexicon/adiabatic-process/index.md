---
title: "adiabatic process"
time: 202305120451
type: lexicon
domain: fluids

aka: 

alsoread: 

what:
  0: |
    a process that's thermally isolated, i.e. the system can't transfer heat or 
    mass with the environment.

    contrast with {{< 词典 isothermal process >}}.

    since an adiabatic process can't transfer heat or mass, the _only_ way it 
    can exchange energy with the surroundings is through work.

  1: |
    a chemical/physical process that happens so fast that there isn't time for 
    energy to be exchanged with the surroundings through heat. the {{< 词典 
    adiabatic approximation >}}.
---
