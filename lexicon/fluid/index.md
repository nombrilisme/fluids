---
time: 202305060415
title: "fluid"
type: lexicon
domain: fluids

what:
  0: |
    matter that can't maintain a static configuration under an applied 
    {{< 词典 shear stress >}} (specifically, this is the main physical property that 
    distinguishes a fluid from a solid)
---
