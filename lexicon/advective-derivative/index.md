---
time: 202304302307
title: "advective derivative"
type: lexicon
aka: 
  - material derivative
  - convective derivative
domain: fluids

what:
  0: |
    the time derivative of a variable that can vary in both time _and_ space. 
    used when discussing the dynamics of flow fields.

    e.g. in fluid dynamics the time derivative of the velocity flow field ˝⬀v = 
    ⬀v(⬀x,t)˝ (which has both spatial and temporal components, and subject to 
    variations in both) is
    ```
      d₍t₎⬀v = 𝝏₍t₎v + (∇⦁⬀v)⬀v
    ```
    the first term describes the rate of change of ˝⬀v˝ in time (in the absence 
    of flow); the second term describes the rate of change of ˝⬀v˝ in space (the 
    transport of the velocity field by the flow).

---
