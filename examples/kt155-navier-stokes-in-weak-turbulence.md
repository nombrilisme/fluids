---
time: 202304251709
title: "navier stokes in weak turbulence"
type: example2
source: 
---

Start with the usual time-dependent navier stokes equation (TDNSE), for a highly 
subsonic flow (∴ incompressible) with no gravity:
```
  𝝏₍t₎v + ∇⦁(v⊗v) = -÷1ρ·p + ν∇²v
  𝓮𝓺{tdnse}
```
where
  - ˝v = v(x,t)˝ is the velocity field; a vector field with spatial components 
    ˝v = [v₍x₎, v₍y₎, v₍z₎]˝
  - ˝p = p(x,t)˝ is the pressure field (scalar field)
  - ˝ρ˝ is the density, assume constant in this case because of 
    incompressibility
  - ˝ν˝ is the kinematic viscosity

In the weak turbulence formalism, you decompose the velocity field ˝v˝ and 
pressure field ˝p˝ into the sum of an average and a fluctuation:
```m
  v = ⟨v⟩ + δv
  p = ⟨p⟩ + δp
```

Substitute these into the TDNSE \eqref{tdnse}, you get
```m
  𝝏₍t₎(⟨v⟩ + δv) + ((⟨v⟩+δv)⦁∇)(⟨v⟩+δv)
  = -÷1ρ (⟨p⟩+δp) + ν∇²(⟨v⟩+δv)
```

Express this in summation notation (only the ˝i˝th component, where ˝i˝ is a sum 
over the ˝x,y,z˝ spatial coordinates):
```m
  𝝏₍t₎(⟨v⟦i⟧⟩) + 𝝏₍t₎(δv⟦i⟧) + ((⟨v⟦j⟧⟩ + δv⟦j⟧) 𝝏⟦j⟧)(⟨v⟦i⟧⟩+δv⟦i⟧)
  = -÷1ρ(𝝏⟦i⟧(⟨p⟩) + 𝝏⟦i⟧(δp)) + ν(𝝏⟦i⟧²(⟨v⟦i⟧⟩) + 𝝏⟦i⟧²(δv⟦i⟧))
```

Since the averages are over time, ˝𝝏₍t₎(⟨v⟩) = 0˝, and for the average "part" of 
the equation you get
```
  (⟨v⟩⦁∇)⟨v⟩ = -÷1ρ ∇⟨p⟩ + ν∇²⟨v⟩ - ∇⦁(δv⊗δv)
```

and for the fluctuating part, 
```m
  𝝏₍t₎(δv) + (⟨v⟩⦁∇)δv + (δv⦁∇)⟨v⟩ + (δv⦁∇)δv -⟨(δv⦁∇)δv⟩⁽?⁾
  = -÷1ρ ∇(δp) + ν∇²(δv)
```


