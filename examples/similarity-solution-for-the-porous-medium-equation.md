---
time: 202212010324
title: "Similarity Solution for the Porous Medium Equation"
type: problem
font: concrete
source: GF101
thumb: GroundwaterMound.jpg
what: 
---

_GF-101_

The porous medium equation (PME):
```
  𝝏₍t₎u = κ·Δ₍d₎·(u⁽1+n⁾)     𝓮𝓺{1011}
```
where
- ˝u = u(r,t)˝
- ˝r = r(x₍1₎, x₍2₎, ..., x₍d₎) ∈ \mathbb R⁽d⁾˝
- ˝Δ₍d₎ = ∑{i=1..d} 𝝏₍x₍i₎₎²˝, or for just the radial component, 
  ˝⌠1⧸r⁽d-1⁾⌡𝝏₍r₎(r⁽d-1⁾𝝏₍r₎)˝

Now, consider a radially symmetric, bell shaped initial condition for ˝u˝, with 
initial width ˝l˝. The "mass" of this distribution is
```
  Q = ∫ r⁽d-1⁾·ɗr·S₍d₎·u(r,t=0)
```
where ˝S₍d₎˝ is the surface area of a unit sphere in ˝d˝ dimensions.

Given a radially symmetric initial condition for ˝u˝, the long time behaviour of 
the PME is of form
```
  u(r,t) ∼ ÷{1}{t⁽dθ⁾}·f(÷{r}{t⁽θ⁾})
```
where ˝θ = ÷{1}{2+nd}˝ and ˝f˝ is the scaling function.

Dimensionally, the solution ˝u˝ depends on the parameters ˝u = f(Q,r,t,l)˝. The 
dimensions of these parameters are
```
  [u] = U
  [r] = L
  [l] = L
  [t] = L⁽2⁾U⁽-n⁾
  [Q] = UL⁽d⁾
```
(sidenote: I'm using the "˝UL˝" system of dimensions here. In Goldenfeld's 
dimensional analysis of the diffusion equation (sec.  10.2.2)[^GFdimanal] and 
Barenblatt's dimensional analysis of the groundwater mound problem (Barenblatt, 
sec. 12.3.2), they both use the "˝ULT˝" system of dimensions, where ˝[t] = T˝.
You may be tempted to omit the constant ˝κ˝ (in the PME) from dimensional 
considerations because the exercise, as written by Goldenfeld, invites the 
reader to assume ˝κ = 1˝ without loss of generality.  Days were spent, wasted, 
by yours truly, trying to figure out what strange manner of mathematical 
trickery they'd used to make the dimensions work out with only ˝Q˝ and ˝t˝, 
because, no matter what happened, there always seemed to be a factor of ˝L˝s 
remaining. Then, when reading the (extremely useful) paper Lin-Yuan, Goldenfeld, 
Oono on RG for the PME[^goodpaper], it hit me that the dimensions of ˝t˝ didn't 
have to be ˝T˝, that you could resolve the ˝κ˝ units into ˝t˝.  Indeed, if you 
include ˝κ˝ in the dimensional considerations, it has
dimensions ˝κ = L⁽2⁾U⁽-n⁾T⁽-1⁾˝.)

[^GFdimanal]: N. Goldenfeld, "Lectures on Phase Transitions and the 
Renormalization Group". Sec 10.2.2. 1992.
[^goodpaper]: L. Chen, N. Goldenfeld, Y. Oono, "Renormalization Group theory for 
the modified porous medium equation". Phys. Rev. A. 1991.

Anyway, if you define the dimensionless groups
```
  Π = ÷{u(Q⁽n⁾t)⁽dθ⁾}{Q}
  Π₍1₎ = ÷{r}{(Q⁽n⁾t)⁽θ⁾}
  Π₍2₎ = ÷{l}{(Q⁽n⁾t)⁽θ⁾}
```
then you can write the solution to the initial value problem in the form
```
  Π = f(Π₍1₎, Π₍2₎)
```

For long times the asymptotic behaviour of the solution is given by the ˝Π₍2₎ → 
0˝ limit (i.e. the ˝l → 0˝ limit, the similarity solution). The similarity 
solution satisfies
```
  Π = f(Π₍1₎)
```

Rearrange this, and conclude that the solution we seek for ˝u˝ can be written
```
  u(r,t) = ÷{Q}{(Q⁽n⁾t)⁽dθ⁾}·f(÷{r}{(Q⁽n⁾t)⁽θ⁾})    𝓮𝓺{1015}
```

To solve for ˝f(ξ)˝, where ˝ξ = r/(Q⁽n⁾t)⁽θ⁾˝, substitute the assumed form 
\eqref{1015} into the PME \eqref{1011}. After some tedious exponent-wrangling 
(not shown, see appendix), you end up with an ODE for the scaling function ˝f˝.

There are some conditions ˝f˝ has to satisfy. Due to the IC, ˝f˝ must be even, 
so ˝f'(ξ = 0) = 0˝. ˝f˝ must vanish monotonically ˝f → 0˝ as ˝x → ∞˝.  Another 
condition is imparted by the conservation law, that the "mass" of the initial 
distribution is conserved, i.e.
```
  m = ∫{-∞}{∞} ɗx u(x,t) = Q ͺͺͺ forͺ t ≥ 0
```
Substitute the assumed form \eqref{1015} into the conservation law, you get 
˝∫{-∞}{∞} ɗξ f(ξ) = 1˝. Solve the ODE for ˝f˝ by integrating once, then, since 
˝f'(ξ = 0) = 0˝, set the constant of integration to zero, and integrate again to 
get an expression for ˝f˝. Now, note that ˝ξ₍0₎˝ is the value of ˝ξ˝ when 
˝𝝏₍t₎u(x,t) = 0˝. Use this fact, along with the other conditions, to match the 
expressions ˝f(ξ = ξ₍0₎) = f'(ξ = ξ₍0₎)˝, then normalise, and you get
```
  f(ξ) = (÷{nθ}{2(n+1)}(ξ₍0₎⁽2⁾ - ξ⁽2⁾))⁽1/n⁾
```
which is valid when ˝ξ < ξ₍0₎˝, and ˝f(ξ) = 0˝ for ˝ξ > ξ₍0₎˝.

Note that if we'd started with the _modified_ porous medium equation (MPME),
```
  𝝏{t}u = Dκ·Δ₍d₎(u⁽1+n⁾)
```
where ˝D˝ is a discontinuous function of ˝𝝏{t}{u}˝,
```
  D = \begin{cases} ÷12 & 𝝏₍t₎u > 0 ,,
    ÷12(1+ε) & 𝝏₍t₎u < 0 \end{cases}
```
then in the case ˝ε ≠ 0˝, there's _no_ similarity solution of form ˝Π = f(Π₍1₎, 
0, Π₍3₎)˝. This is because the limit ˝Π₍2₎ → 0˝ is not regular, and the 
assumption of intermediate asymptotics of the first kind (IA1K) fails. This also 
implies the "mass" integral, ˝Q = ∫ r⁽d-1⁾ ɗr S₍d₎ u(r,t)˝ is not a constant of 
motion. Physically, it's fairly easy to see why this is so. If you consider the 
system at some time ˝t > 0˝, where the mass of the distribution is ˝Q₍1₎˝, then 
there's no unique value for the initial condition that could possibly give rise 
to that state. Since the observed mass ˝Q₍1₎˝ and the initial mass ˝Q˝ have the 
same dimensions, they must be proportional, so ˝Q₍1₎ = G⁽-1⁾Q˝. The 
dimensionless constant ˝G˝ must depend on the initial width ˝l˝, in order that 
˝Q₍1₎˝ is independent of ˝l˝. This means ˝G˝ must be a function of form ˝G = 
G(l/μ)˝, where ˝μ˝ is an arbitrary length scale that enters the problem.

{{< figure "blackboard/SettlingOfTheMound.jpg" >}}
