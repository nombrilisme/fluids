---
time: 202301311706
title: "the pressure on an infinitesimal cube of liquid"
type: problem
source:
colour: greek
thumb: pressureoncubewater.jpg
wide: true
---

{{< figure pressureoncubewater.jpg 10 >}}
