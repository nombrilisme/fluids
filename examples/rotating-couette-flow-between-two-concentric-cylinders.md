---
time: 202304200956
title: "rotating Couette Flow between two concentric cylinders"
type: problem
font: concrete
source: 
what: |
  There are two concentric cylinders. The inner one rotates and the outer one 
  doesn't. There's a liquid between the cylinders. You gradually decrease the 
  viscosity of the liquid by heating it (causing ˝Re˝ to increase).
---


