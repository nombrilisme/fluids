---
time: 202303241300
title: "3 examples of 2D Vorticity"
type: problem
domain: fluids
colour: overcast
wide: true
thumb: 2d-vorticity-examples.svg
source: 
---

{{< figure 2d-vorticity-examples.svg >}}
