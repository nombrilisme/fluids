---
time: 202308281023
title: "ce2 equations"
type: note
domain: fluids
---

# DSS + CE2
--------------------------------------------------------------------------------

{{< P >}}
  first and second cumulants:
  ```
    ⬀c(⬀r) = ⟨⬀u(⬀r)⟩
  ```
  ```
    ⬀c(⬀r₁,⬀r₂) = ⟨⬀u'(⬀r₁) ⊗ ⬀u'(⬀r₂)⟩
  ```

{{< P >}}
  EOMs for the cumulants:
  ```
    𝝏₍t₎⬀c(⬀r₁) 
    = L(⬀c(⬀r₁)) + Q(⬀c(⬀r₁),⬀c(⬀r₂))
      + ∫ d⬀r₂ ͺ Q(⬀c(⬀r₁,⬀r₂), δ(⬀r₁-⬀r₂) ⬈I)
  ```
  ```
    𝝏₍t₎⬀c(⬀r₁,⬀r₂) = L₍⬀c(⬀r₁)₎(⬀c(⬀r₁,⬀r₂)) + L₍⬀c(⬀r₂)₎(⬀c(⬀r₁,⬀r₂))
  ```
