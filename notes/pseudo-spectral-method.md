---
time: 202308281025
title: "pseudo spectral method"
type: note
domain: fluids
---

{{< P >}}
  finite element method: divides interval into sub intervals, solves the PDEs 
  locally at neighbouring intervals.

{{< P >}}
  spectral methods: discretise variables by expand over the entire domain using 
  global basis functions. the solution is the set of coefficients for the basis.

{{< P >}}
  start: choose a set of basis functions that span the domain. for pipe flow, 

  choose fourier series for the periodic domain (azimuthal direction θ) and the 
  streamwise (axial) direction z.

  use chebyshev series for the nonperiodic (radial) direction r.

{{< P >}}
  pure spectral method: calculates the nonlinear terms in the fourier space 
  representation. this is expensive.

{{< P >}}
  pseudo spectral: not purely spectral. evaluates nonlinear terms in real space, 
  then transfers back to spectral space for next time step.

{{< P >}}
  PS divides domain into sub intervals. dividing points: COLLOCATION POINTS. you 
  solve the pde on the collocation points. not necessarily evenly spaced. higher 
  density at ends to resolve sharp features around the boundary.

{{< P >}}
  consider an orthonormal basis set ˝[φ⟦n⟧(x)]˝. 

  a function ˝f(x)˝ can be represented in this basis as:
  ```
    f(x) = ∑{n=0..∞} f⟦n⟧ φ⟦n⟧(x)
  ```

  where ˝f⟦n⟧ = ⟨φ⟦n⟧(x) ∤ f(x)⟩˝ is the inner product of ˝φ⟦n⟧(x)˝ and ˝f(x)˝.

{{< P >}}
  numerically you approximate the functions with a truncated set of finite basis 
  functions and their values at collocation points:
  ```
    f(x) ≈ ∑{n=0..(N-1)} f⟦n⟧ φ⟦n⟧(x⟦n⟧)  
  ```
  ```
    f⟦n⟧(x) = ∑{n=0..(N-1)} w⟦n⟧ f(x⟦n⟧)
  ```

{{< P >}}
  the spectral coefficients decay exponentially for smooth functions. this makes 
  the results accurate.
