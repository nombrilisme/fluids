---
title: "the quasilinear approximation"
time: 202306241528
type: note
domain: fluids
---

# DSS AND CE2
--------------------------------------------------------------------------------

{{< P >}}
  DSS (direct statistical simulation) means finding solutions to 
  equations that describe the *statistical* properties of a system {{< ℭ MTIAT 
  >}}. you have to statistically *truncate* the system {{< ℭ CZ ch1.1 >}}.

{{< P >}}
  CE2 is an example of DSS. you do a direct cumulant expansion and truncate at 
  the second order {{< ℭ CZ ch1.1 >}}.

{{< P >}}
  numerical simulation vs DSS: little diagram showing the hierarchy of 
  approximation schemes for numerical simulation and the equivalent DSS method:

  {{< figure numerical-vs-dss-evolution.jpg >}}

  {{< ℭ MTIAT >}}

{{< P >}}
  benefits of CE2: has exact closure in the QL approximation.

{{< P >}}
  QL is a mean field theory: it decomposes the equation into a mean field and a 
  perturbation field {{< ℭ CZ >}}.

{{< P >}}
  triad diagrams in wavenumber space. ˝m˝ is the wavenumber. the straight line 
  with ˝m=0˝ is the mean flow.

  {{< figure ql-triad-diagrams.png >}}
  - left: wave-mean flow interaction
  - middle: reynolds forcing of mean flow
  - right: wave-wave interaction. omitted in the QL approximation.

  {{< ℭ MTIAT >}} {{< ℭ CZ >}}

{{< P >}}
  two consequences of omitting the wave-wave interactions: 

  1. there are no scatterings to higher wavenumbers. either the scatterings 
  between waves stay within their own wave modes OR they get reduced to the mean 
  flow.

  2. the QL system is closed, so energy is conserved.


# QL EQUATIONS OF MOTION
--------------------------------------------------------------------------------

{{< P >}}
  form of the EOM:
  ```
    𝝏₍t₎⬀u = L(⬀u) + Q(⬀u, ⬀u)
  ```
  - ˝L(ͺ)˝ is a linear operator
  - ˝Q(ͺ,ͺ)˝ is a nonlinear quadratic operator

{{< P >}}
  do a reynolds decomposition of the field into a mean and fluctuating part
  ```
    ⬀u = ⟨⬀u⟩ + δ⬀u
  ```

  the averaging operator satisfies
  ```m
    ⟨⟨⬀u⟩⟩ = ⟨⬀u⟩
    ⟨δ⬀u⟩ = 0
    ⟨⟨⬀u⟩ ⬀u⟩ = ⟨⬀u ⬀u⟩
  ```

{{< P >}}
  the averaging operator could average over space, ensembles, or time. but 
  spatial averaging is the most common. ensemble averaging takes the most time 
  {{< ℭ CZ >}}.

{{< P >}}
  after doing the reynold's decomposition, the EOM form for the mean field is:
  ```
    𝝏₍t₎⟨⬀u⟩ = L(⟨⬀u⟩) + Q(⟨⬀u⟩,⟨⬀u⟩) + ⟨Q(δ⬀u,δ⬀u)⟩
  ```

  and for the fluctuating field:
  ```
    𝝏₍t₎(δ⬀u) = L(δ⬀u) + Q(⟨⬀u⟩,δ⬀u) + Q(δ⬀u,⟨⬀u⟩) + ( Q(δ⬀u,δ⬀u) - ⟨Q(δ⬀u,δ⬀u)⟩ )
  ```
  the term in parens represents the *wave + wave → wave scattering* 
  interactions. in the QL approximation you omit this term. we're in the limit 
  when this scattering term is small compared to the first three terms.

  so the QL EOMS are:
  ```
    𝝏₍t₎⟨⬀u⟩ = L(⟨⬀u⟩) + Q(⟨⬀u⟩,⟨⬀u⟩) + ⟨Q(δ⬀u,δ⬀u)⟩
  ```
  ```
    𝝏₍t₎(δ⬀u) = L(δ⬀u) + Q(⟨⬀u⟩,δ⬀u) + Q(δ⬀u,⟨⬀u⟩)
  ```

