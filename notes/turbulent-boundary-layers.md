---
time: 202305021226
title: "turbulent boundary layers"
type: note
---

# recap: blasius profile for a laminar boundary layer
--------------------------------------------------------------------------------


the thickness of the boundary layer, as measured distance a ˝x˝ downstream from 
the start of the boundary layer, is
```
  3δ = 3(⌠νx⧸v₍ups₎⌡)¹𝄍²
```

when ˝Re˝ is sufficiently large, i.e. ˝Re = ⌠v₍ups₎d⧸ν⌡ ∼ 3⋄10⁵˝ or ˝Re₍δ₎ ∼ 
√{Re₍d₎} ∼ 500˝ for flow past a cylinder, the boundary layer becomes turbulent.


# composition of a turbulent boundary layer
--------------------------------------------------------------------------------

a turbulent boundary layer is comprised of:
- a THIN LAMINAR SUBLAYER, thickness ˝δ₍ls₎˝, which is CLOSE to the wall
- a thick turbulent zone, thickness ˝δ₍t₎˝

# assumptions
--------------------------------------------------------------------------------

ASSUMPTION: the wall is locally flat (planar). the wall can be any shape, but 
its radius of curvature must be large compared to the thickness of the boundary 
layer

coordinates: cartesian, with
- ˝y˝ perpendicular to the boundary
- ˝x˝ is measured _along_ the boundary

# schematic view of the boundary layers
--------------------------------------------------------------------------------

{{< figure TurbulentBoundaryLayerSchematic.svg >}}

# structure of the turbulent boundary layer
--------------------------------------------------------------------------------

IN THE TURBULENT REGION, ˝T₍xy₎˝ is equal to the reynolds stress, i.e. ˝ρv₍l₎²˝.

˝v₍l₎˝ is the turbulent velocity of the largest eddies at distance ˝y˝ from the 
wall.

the fact that ˝T₍xy₎˝ is constant means ˝v₍l₎˝ is constant.

the largest eddies at ˝y˝ have size ˝l˝ of order of the distance ˝y˝ from the 
wall. 

so the TURBULENT VISCOSITY is
```
  ν₍t₎ ∼ ÷13 v₍l₎ y
```

the reynolds stress can be expressed
```
  Reynoldsͺstress = ρv₍l₎² = 2ρν₍t₎½⟨v₍y₎⟩
```
where
- ˝⟨v⟩˝ is the mean flow speed
- ˝½⟨v₍y₎⟩˝ is the shear

using these definitions, the MEAN FLOW SPEED varies logarithmically with 
distance from the wall
```
  ⟨v⟩ ∼ v₍l₎ ln(y) + const
```

the turbulence is created at the inner edge of the turbulent zone, where ˝y ∼ 
δ₍ls₎˝. and remember: it's created by the interaction of the mean flow with the 
laminar sublayer. 

so the largest turbulent eddies, at must have their turnover speeds ˝v₍l₎˝ equal 
to the mean flow speed
```
  ⟨v⟩ ∼ v₍l₎ ͺͺͺ atͺy ∼ δ₍ls₎
```

so the profile of the mean flow speed in the turbulent zone:
```
  ⟨v⟩ ∼ v₍l₎ (1 + ln(y/δ₍ls₎)) ͺͺͺatͺy ≥ δ₍ls₎
```

# structure of the laminar sublayer
--------------------------------------------------------------------------------

the constant shear stress is viscous, so 
```
  T₍xy₎ = ρν ⟨v₍y₎⟩
```

balance of stress at the interface between the laminar and turbulent zone means

the mean flow speed in the laminar sublayer:
```
  ⟨v⟩ ∼ v₍l₎ (÷{y}{δ₍ls₎}) ͺͺͺatͺy ≤ δ₍ls₎ ∼ ν / v₍l₎
```

# the turbulent velocity of the largest eddies in the turbulent zone
--------------------------------------------------------------------------------

you match the ˝v₍l₎˝ to the free streaming region outside it, ˝v₍ups₎˝.

introduce a reynolds number for the boundary layer:
```
  Re₍δ₎ = ÷{v₍ups₎δ₍t₎}{ν}
```

so
```
  v₍l₎ ∼ ÷{v₍ups₎}{ln(Re₍δ₎)}
```

# evolution of the boundary layer thickness ˝δ₍t₎˝ down the wall
--------------------------------------------------------------------------------

key mechanism: entrainment.

at the outer part of the turbulent zone, the largest eddies move with speed 
˝v₍l₎˝ into the free streaming fluid. this causes entrianment of the fluid into 
the turbulent eddies.

the thickness grows at rate
```
  d₍x₎δ₍t₎ ∼ ÷{v₍l₎}{v₍ups₎} ∼ ÷1{ln(Re₍δ₎)}
```

# separation in a turbulent boundary layer
--------------------------------------------------------------------------------

two conclusions:
- the turbulent boundary layer expands faster than the laminar boundary layer 
  would
- the turbulent layer is thicker at all locations down the wall

in a long straight pipe, the drag on the pipe wall increases when the boundary 
layer becomes turbulent.

but for flow around a cylinder, the drag goes down.

nonobvious conclusion: if your goal is to reduce drag, AND the laminar flow is 
prone to separation, a nonseparating turbulent layer (or delayed separation) is 
better than a laminar layer.

"vortex generators" --- causes the turbulent boundary layer to stick to the 
surface more effectively than the laminar boundary layer does, because of the 
conda effect.
