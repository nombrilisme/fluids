---
time: 202304041741
title: "vorticity"
type: note
domain: fluids
#colour: overcast
showsummary: T
---

# DEFINITIONS
--------------------------------------------------------------------------------

{{< P >}}
  define vorticity, ˝⬀ω˝, the curl of the velocity field ˝⬀v˝:
  ```
    ⬀ω = ∇ × ⬀v
  ```

{{< P >}}
  vorticity is a vector field. this means you can describe a set of integral 
  curves, called {{< 词典 vortex lines >}}, by solving ˝d⬀x/dλ = ⬀ω˝ for a
  parameter ˝λ˝.

{{< P >}}
  the flux of vorticity across a surface ˝S˝:
  ```
    ∫₍S₎ ⬀ω ⦁ d⬀A
  ```
  and by stokes theorem this is also equal to the line integral of the velocity 
  field along the boundary of the surface ˝𝝏S˝:
  ```
    ∫₍𝝏S₎ ⬀v ⦁ d⬀x
  ```
  this is the {{< 词典 circulation >}} around ˝S˝, denoted ˝Γ˝.

{{< P >}}
  vorticity has zero divergence: 
  ```
    ∇⦁⬀ω = 0
  ```
  this implies that vortex lines are continuous. note the similarity to magnetic 
  fields and field lines.


# 2D VORTICITY
--------------------------------------------------------------------------------

{{< P >}}
  decomposition of vorticity for 2d flows: recall that ˝⬀ω = ∇×⬀v˝, so in 2D 
  (taking only the z-component) you can express vorticity as the scalar quantity
  ```
    ω = (𝝏₍x₎v₍y₎ - 𝝏₍y₎v₍x₎)⇧e₍z₎
    𝓮𝓺{scalarvorticity}
  ```
  since it's characterised purely by its magnitude, not direction. often called 
  {{< 词典 scalar vorticity >}}.

{{< P >}}
  the vorticity at a point is the sum of the angular velocities of two 
  perpendicular lines passing through that point. this is implied by equation 
  \eqref{scalarvorticity}.

  this means you can "measure" 2d vorticity by placing a "vane" with 2 
  orthogonal fins into the vortical flow, and observing its rate of rotation. 
  the nature of the flow (see examples below) will impart an angular velocity to 
  each fin of the vane. the total angular velocity of the vane is the average of 
  the angular velocities on each fin. {{< 𝖗 TB p731 >}}

  {{< figure VaneInVorticalFlow.png >}}



{{< P >}}
  uniformly rotating flow: a 2d flow around a point with constant angular 
  velocity ˝⬀Ω˝. the vorticity is ˝2⬀Ω˝ everywhere.

  {{< figure 2dFlowConstantAngMom.png >}}

  the angular velocity ˝⬀Ω˝ is the same at all points in this flow.

  velocity field:
  ```
    ⬀v = ⬀Ω × ⬀r
  ```
  where ˝⬀r˝ is the radial vector. 

  vorticity:
  ```
    ⬀ω = 2⬀Ω ͺͺ∀ͺ⬀r
  ```

{{< P >}}
  spiralling inward flow: a 2d flow with constant angular momentum per unit 
  mass, ˝⬀j = j⇧z˝. the vorticity is zero everywhere except the centre of 
  rotation.

  {{< figure 2dFlowConstantAngMomPerUMass.png >}}

  here the angular momentum and angular velocity changes radially. this is the 
  kinda of vorticity in a tornado or in a drain.

  velocity field:
  ```
    ⬀v = ⬀j × ⌠1⧸r²⌡⬀r
  ```
  where ˝r = |⬀r|˝ is the radius.
  
  vorticity:
  ```
    ⬀ω = 2πjͺδ(⬀r)
  ```
  where ˝δ(⬀r)˝ is the 2d dirac delta function (zero everywhere except the 
  centre of rotation, where ˝⬀r = 0˝)

  why the vorticity zero everywhere: two adjacent fluid elements separated 
  _tangentially_ rotate around each other with angular velocity ˝⬀j/r²˝. two 
  adjacent fluid elements separated _radially_ rotate around each other with 
  angular velocity ˝-⬀j/r²˝. the average of the two angular velocities is zero. 

{{< P >}}

  {{< figure ShearingFlowLaminarBoundaryLayer.png >}}



# VORTICITY EVOLUTION
--------------------------------------------------------------------------------
