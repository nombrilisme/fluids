---
time: 202305181222
title: "DSS"
type: note
domain: fluids
draft: true
---

{{< P >}}
  kraichnan: qui êtes-vous?

{{< P >}}
  in kraichnan's theory of a statmech of turbulence, there are big differences 
  between 2d flows and 3d flows.

{{< P >}}
  2d flows have frequent emergence of coherent structures. these coherent 
  structures have two types: vortices and jets.
  
{{< P >}}
  objective of paper: investigate a simple model of 2d fluid flow that exhibits 
  a transition between jets and vortices.

{{< P >}}
  DSS: a set of tools to describe the statistics of turbulent flows. it doesn't 
  use the "traditional" way of collecting statistics used by DNS (e.g. mean flow 
  and 2 point correlation function). ∴ DSS can access regimes not accessible 
  through DNS.

{{< P >}}
  kraichnan: focuses on flows with isotropic and homogeneous statistics. seminal 
  paper in 1967: statistical desc of foward/inverse cascades of energy between 
  different scales.

{{< P >}}
  translational and rotational symmetries reduce the technical complexity of 
  statistical theories.

  most fluid flows in nature are both anisotropic and hetrogeneous.

  in DSS this is a feature, not a bad thing. anisotropy and inhomogeneiy can 
  lessen the nonlinearity of flows. they can make the statistics accessible to 
  perturbative computation.

{{< P >}}
  MT: show that a simple version of DSS where the eoms for the spatially 
  averaged statistics are CLOSED at the level of 2nd order moments or cumulants. 
  can reproduce many elements of the 2D flow.
