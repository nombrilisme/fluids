---
time: 202307121245
title: "ecc notes"
type: note
domain: fluids
---

# finding a minimal model
--------------------------------------------------------------------------------

{{< P >}}
  {{< S 4-point pca method for finding the minimal model >}}. that produces the 
  transition to turbulence in an annular pipe flow.

  1. the principal components in QL is the fourier basis along the θ (averaging) 
  direction. plot the cumulative explained variance (the cumulative power 
  spectrum over the θ direction).

  2. choose the reduced model based on how much variance to keep in the system. 

  3. plot the cumulative power spectra over the other two (r and z) directions. 
  use this to decide how much resolution to keep in the reduced model. 

  4. simulate over a range of reynolds numbers to look for critical behaviour. 
  if you find the critical point (transition), go down to a more reduced model 
  and keep doing this until you find the minimal model.


the most truncated system: Nr=50 Nθ=4 Nz=128. with m=0, ±1.

for Re upto 2250 the turbulence remains localised - a puff. 

at larger flow rates the puffs delocalise by splitting into two or more. 

at much larger Re, around 2800, the disturbances develop into a rapidly 
expanding active region of turbulence -- a slug.

start with minimal model. one mode only, m=3. kerswell. 
IC: lots of turbulence. see if it holds if Re increases.


# kerswell
--------------------------------------------------------------------------------

do relaminarisation stats for turbulent puffs differ in character between short 
and long pipes. do long pipe sims have a critical Re for sustained puffs, but 
not for short pipes?

what does the attracting edge state in the laminar turbulent boundary look like 
in a long pipe? localised like a piff? does it delocalise at the same Re as a 
puff?

previously: subcritical dynamics of pipe flow not retained.

search for evidence of turbulent transients. 

kerswell: new model with hi res in cross stream and streamwise directions, but 
small modes in spanwise (azimuth) directions. to preserve high radial 
resolution.

only fourier modes m=0, ±m₀ were considered.

localised disturbances found at low re 2600 and delocalist at intermediate re 
2300 and expand into slugs at high re 4000.
