---
time: 202308241704
title: "transition to turbulence in pipes"
type: note
domain: fluids
---

{{< P >}}
  in pipes, the flow transitions from laminar to turbulent when you increase the 
  flow speed.

{{< P >}}
  during the transitional regime the flow develops regions of *localised 
  turbulence* called puffs.

{{< P >}}
  right now there isn't a good field theory that describes the transition to 
  turbulence. the NS equation is good at describing fluid motion at the 
  microscopic level, but not the transition.

{{< P >}}
  the reynolds number, Re. reynolds observed that in pipes, the transition 
  depends on a dimensionless parameter:
  ```
    Re = ⌠1⧸μ⌡ ρuL
  ```
  or
  ```
    Re = ⌠1⧸ν⌡ LV
  ```
  - ˝μ˝: dynamic viscosity (absolute viscosity)
  - ˝ν˝: kinematic viscosity
  - ˝ρ˝: density
  - ˝L˝: characteristic length scale
  - ˝u˝: bulk velocity (cross-sectionally averaged mean flow speed)
  - ˝V˝: characteristic flow velocity

  as you increase Re, the flow eventually transitions from laminar to turbulent.

{{< P >}}
  if the pipe flow is driven by pressure, you need to model how much pressure 
  you need to induce the transition to turbulence.

{{< P >}}
  the transition to turbulence in terms of the friction factor:
  ```
    f = ⌠(Δp)d ⧸ ½ρL₀u²⌡
  ```
  - ˝d˝: pipe diameter
  - ˝ρ˝: density
  - ˝u˝: bulk velocity
  - ˝L₀˝: stremwise distance between pressure differential (the length across 
    which the pressure gradient exists).

  {{< figure friction-factor-vs-Re.png >}}
  - green dots: experimental data when a transition is observed
  - white dots: what you *could* observe if the experiment is very highly 
    controlled to minimise disturbances. the flow stays laminar even as Re 
    increases.

  at low Re, the flow is laminar. the friction factor follows the scaling 
  relationship derived from the NSE. at high Re the flow is turbulent, and the 
  friction factor follows prandtl's law. 

  at a given Re (in the transitional region) the friction factor in the 
  turbulent state is higher than in the laminar state. this is because friction 
  drag increases in the fluid when turbulent motion is present.

{{< P >}}
  if you have a laminar flow in an environment very free of disturbances (the 
  disturbances that trigger turbulence) then the laminar friction law extends to 
  higher Re.

  what this means is that turbulence *requires* a finite amount of disturbance 
  to get triggered. the transition is *subcritical*.

  there are numerical and experimental sims that show you can maintain stable 
  laminar pipe flow to infinitesimal disturbances up to a certain Re.

{{< P >}}
  the transition to turbulence is not really a phase transition, in the sense of 
  statistical mechanics. because the system is is non-equilibrium.

  numerical and experimental sims: full turbulence *arises from puffs* which 
  split up and eventually take over the whole domain.

{{< P >}}
  as the system evolves, puffs either die or split up. 

  W/K: studies on the lifetime of puffs. observed that the mean lifetime of a 
  puff, ˝τ˝, is a double exponential of the Re:
  ```
    ln(ln(τ)) = Re
  ```
  or
  ```
    τ = exp(exp(Re))
  ```

  this suggests the entire phenomenon of turbulence is just a *transient state* 
  of a very long survival time. we don't yet know how exactly this works, and 
  how it relates to the NSE.

{{< P >}}
  pomeau: the laminar state is an *absorbing state*, and the laminar → 
  turbulence transition is governed by a process in the DP universality class.

  DP is a class of models that mimic the filtering of fluids through porous 
  materials. analogy: a fluid trying to flow through a series of bonds/barries 
  that have different connectivities.

  the penetration probability: probability that the bond is OPEN to let the 
  fluid pass through. 

  the value of the penetration probability can cause the system to have a phase 
  transition from a *macroscopically permeable state* (fluids can readily pass 
  through the pores) to a *macroscopically impermeable state* (fluids can't pass 
  through the pores). obviously, the impermeable state strongly restricts fluid 
  flow.

  the macroscopically IMPERMEABLE state: analogue of the laminar state.

  when you change the Re, the penetration probability changes, and the system 
  goes from the turbulent state (puff survival) to the laminar state (puff 
  extinction).

{{< P >}}
  CZ's work: use the QL approximation as a bridge find a link between the 
  transition to turbulence and statistical mechanics.

{{< P >}}
  la problème: we've observed experimentally that laminar flow is stable to 
  infinitesimal disturbances at Re way above the transitional region (up to a 
  certain Re, that is).

{{< P >}}
  in 1986 pomeau suggested the transition is part of the DP universality class. 
  means the transition is continuous wrt the penetration probability.

  BUT experiments showed a discontinuity in the order parameter in plane couette 
  flows (contradicting DP).

  in 2011 avila found that turbulence can be induced by spatial coupling of 
  chaotic domains. thus you need large enough domain to actually observe the 
  critical behaviour in the transition.

{{< P >}}
  in 2016: shih et al showed a relation between the L-T transition and DP. 
  linked pipe flow system with predator prey system.

  the PP system has a phase transition in the DP universality class at the limit 
  of prey extinction. a zonal flow emerges because of the presence of turbulent 
  flow. 

  the zonal flow is the predator. the turbulent flow is the prey. the laminar 
  flow is the nutrient.
