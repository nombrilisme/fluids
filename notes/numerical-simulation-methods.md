---
time: 202308281013
title: "numerical simulation methods"
type: note
domain: fluids
---

# DNS
--------------------------------------------------------------------------------

{{< P >}}
  high fidelity solutions of turbulent flows. explicitly resolves the dynamics 
  (rather than modeling the flow with a reynolds-averaged NS closure). ALL 
  scales are resolved.

# DSS
--------------------------------------------------------------------------------

{{< P >}}
  you don't resolve the system in all scales. instead you solve the statistics 
  directly. don't need to resolve microscopic structure.

{{< P >}}
  main method: simulate the low order statistical moments or cumulants. requires 
  closure in high order terms. 

{{< P >}}
  CE2 is exact in QL. this means you can do DSS in QL.

# DSS + CE2
--------------------------------------------------------------------------------

{{< P >}}
  first and second cumulants:
  ```
    ⬀c(⬀r) = ⟨⬀u(⬀r)⟩
  ```
  ```
    ⬀c(⬀r₁,⬀r₂) = ⟨⬀u'(⬀r₁) ⊗ ⬀u'(⬀r₂)⟩
  ```

{{< P >}}
  EOMs for the cumulants:
  ```
    𝝏₍t₎⬀c(⬀r₁) 
    = L(⬀c(⬀r₁)) + Q(⬀c(⬀r₁),⬀c(⬀r₂))
      + ∫ d⬀r₂ ͺ Q(⬀c(⬀r₁,⬀r₂), δ(⬀r₁-⬀r₂) ⬈I)
  ```
  ```
    𝝏₍t₎⬀c(⬀r₁,⬀r₂) = L₍⬀c(⬀r₁)₎(⬀c(⬀r₁,⬀r₂)) + L₍⬀c(⬀r₂)₎(⬀c(⬀r₁,⬀r₂))
  ```
