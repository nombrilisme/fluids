---
title: "the forces in a fluid"
time: 202304171346
type: note
domain: fluids
---

# STRESS
--------------------------------------------------------------------------------

{{< P >}}
  define {{< lex stress >}}: a quantity that describes all the internal forces 
  acting within a body that cause it to deform. 

{{< P >}}
  mathematically {{< lex stress >}} is defined as a force per unit area. its 
  dimensions are ˝[σ] = L⁽-1⁾ M T⁽-2⁾˝ and in SI units ˝Pa = kg m⁽-1⁾ s⁽-2⁾˝

{{< P >}}
  some examples of stress: tension, compression, torsion, {{< lex shear >}}.

  {{< figure tension-compression.png 6 >}}
  {{< figure shear.png 6 >}}
  {{< figure torsion.png 6 >}}

{{< P >}}
  don't confuse stress with {{< lex strain >}}, which is a quantity that 
  describes the actual deformation of a body (see below).


{{< P >}}
  compressive stress: is when forces (like tension or compression) cause an 
  object to become compressed.
  - the bigger these forces and the smaller the cross sectional area, the 
    greater the stress.
  - is why stress is defined as force per unit area

# STRAIN, DEFORMATION
--------------------------------------------------------------------------------

¶ strain is a measure of deformation of an object. it describes the displacement 
  between particles relative to a reference length

¶ functional form of deformation:
  ```
    ↗x = F(↗x')
  ```
  - ↗x' is an array of reference positions for points of the body
  - deformations have units of length

¶ strains are usually dimensionless

¶ whenever a solid material undergoas a strain (deformation), there is an 
  internal elastic stress that causes the object to restore to its original 
  non-deformed state (like the spring force)

¶ in liquids and gases, ONLY deformations that change the volume will generate a 
  persistent elastic stress
  - if the deformation changes gradually, in fluids there will be some viscous 
    stress that opposes the change.

¶ elastic and viscous stresses are both kinds of mechanical stress.

# SHEAR FORCE AND SHEAR STRESS
--------------------------------------------------------------------------------

{{< 𝖉𝖊𝖋 SHEAR FORCES >}}
  forces that acts on one part of a body in a certain direction, and another 
  part in the opposite direction
{{< /𝖉𝖊𝖋 >}}

¶ if the forces are aligned (collinear), then they are just tension or 
  compression forces

{{< 𝖉𝖊𝖋 SHEAR STRESS τ >}}
  the component of stress that's coplanar with the body's cross section
  ```
    τ = ÷FA
  ```
  - ˝A˝ is the cross sectional area of the material that's parallel to the 
    applied force
  - points are coplanar if there's a plane that contains them all
{{< /𝖉𝖊𝖋 >}}

link: {{< ℑ shear stress >}}

# WHAT DISTINGUISHES A FLUID
--------------------------------------------------------------------------------

¶ the main difference between a fluid and solid: a fluid can't maintain a shear 
  stress for any length of time. 
  - ie if you apply a shear to a fluid, it'll move
  - some fluids move more easily under shear than others

¶ viscosity: property that describes how easily a fluid yields (under a shear 
  stress)

