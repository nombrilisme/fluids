---
time: 202308281047
title: "ql pipe simulation of transition"
type: note
domain: fluids
---

{{< P >}}
  key observations in QL simulation: 
  - can observe the transition
  - observe the puff state (transitional) and the fully turbulent state.

{{< P >}}
  simulation:
  - increase the imposed pressure. the puff splits into smaller puffs and 
    eventually fills the whole pipe.

{{< P >}}
  Re of QL sims are higher than FNL for the same steady state. because of 
  absence of wave + wave -> wave scattering modes. so you need more external 
  forcing to achieve turbulence without this higher order scattering.

{{< P >}}
  difference between FNL and QL: spectra over Kz are similar. spectra over m are 
  very different. 

  for each steady state QL spectra show lots of energy accumulation in the first 
  few modes, compared to FNL. 

  because in the QL, KE is confined in the first few modes because it can't 
  scatter to higher wave modes through the absent w+w→ws modes. 

  energy is conserved, not cut out, in QL.

{{< P >}}
  puffs are pretty much the same in FNL and QL, but QL has more energy 
  accumulation in the m spectrum because of the absence of wwws modes.

{{< P >}}
  in QL the transitional state occurs at higher Re. also requires more 
  excitation in the initial state.

  CZ: IC of QL sims generated with Re=10,000. enough disturbance to trigger 
  turbulence.

{{< P >}}
  conclusion: can capture the transitional state to turbulence with QL. 

  even though the ToT in pipes is subcritical, the WWWS scatterings aren't 
  needed for the transition.

  suggested path to model reduction: look at the steep cut off in the m spectra.

{{< P >}}
  calculation of turbulent state: condition
  ```
     ⌠ ν√{u₍θ₎² + u₍r₎²} ⧸ 4P'(r₍out₎-r₍in₎)² ⌡
  ```
  threshold: when this condition is > 0.0003. 

{{< P >}}
  turbulent fraction: volume fraction of pipe that is in the turbulent state.

{{< P >}}
  critical Re: the lowest Re with a nonzero turbulent fraction.

{{< P >}}
  reduced Re:
  ```
    ⌠Re - Re₍c₎⧸Re₍c₎⌡
  ```

{{< P >}}
  plot log-log relation of TF as a function of RRe.

{{< P >}}
  tft you don't need the WWWS modes for the transition means the mean flow 
  contributes to the ToT.

{{< P >}}
  QL setup: (maintain same as CZ for now)
  - annular pipe
  - inner radius 0.5
  - outer radius 1
  - length, L = 1
  - BCs on inner and outer walls: no-slip
  - BCs on ends of pipe: periodic
  - also the azimuthal direction θ is periodic anyway.

  since θ and z have periodic BCs, use F series as the basis for these two 
  directions, and use chebyshev for the radial direction.

  drive the flow with an imposed pressure in the z direction. you change the 
  imposed pressure to vary the Re.

  CZ's FNL resolution: rθz = 96,256,512.

  QL: apply in the θ direction (averaging only over θ), so much fewer modes are 
  required in the θ direction. set resolution to rθz = 100,128,512. with this 
  resolution can get DP scaling.

{{< P >}}
  puff elongates as you reduce the system. see this by plotting the streamwise 
  vorticity. 
