---
time: 202304141241
title: "turbulence"
type: long
---

# RECAP: LAMINAR PIPE FLOW
--------------------------------------------------------------------------------

¶ the flow velocity profile is parabolic (across the cylindrical cross section). 
  you can achieve this form by balancing the stress across the surface.
  - pouseuille formula for flow of a viscous fluid down a pipe, assuming 
    laminar, sec 13.7.6

¶ the rate of flow is proportional to the 4th power of pipe diameter:
```
  flowͺrateͺ(volumetricͺflux) ∝ d⁽4⁾
```

¶ reynolds number:
```
  Re₍pipe₎ = ÷1ν ⟨v⟩d
```

¶ experimentally pipe flow is only laminar up to a critical Re number, 
  ˝∼10⁽3⁾-10⁽5⁾˝.  the exact value depends on smoothness of the walls and the 
  pipe entrance.


¶ turbulence: if you increase the Re number past a critical threshold (e.g. by 
  increasing the pressure gradient, thereby increasing ⟨v⟩) then the velocity 
  field becomes irregular, spatially and temporally. you get turbulence.

# FLOW PAST A CYLINDER: SETUP
--------------------------------------------------------------------------------

Consider a cylinder placed perpendicular to a flow. Assume ˝⬀v˝ is small 
compared to the speed of sound, so you can ignore compressibility. Assume 
gravity is negligible. 

Parameters:
- ˝d˝, the diameter of the flow (and ∴ characteristic length scale of the 
  problem)
- ˝V˝, the velocity of the flow far before it reaches the cylinder ("upstream 
  velocity").
- ˝ρ˝, density
- ˝ν˝, kinematic viscosity

## REYNOLDS NUMBER

you can use the the upstream velocity is the characteristic velocity, so the 
reynolds number is
```
  Re = ÷1ν Vd
```

## MACH NUMBER (if compressible)

if you had a flow where the sound speed ˝c₍s₎˝ is sufficiently high to to 
invalidate incompressibility, then you can define the mach number (another 
dimensionless number)
```
  M = ⌠V⧸c₍s₎⌡
```

## EQUATIONS - stationary initial flow

Suppose that initially the flow is stationary. In this case you can describe the 
flow with the usual equations for stationary incompressible flow:
```
  ∇⦁⬀v = 0
```
(incompressibility)
```
  (⬀v⦁∇)·⬀v = -÷1ρ·∇P + ν·∇⁽2⁾·⬀v
```
(time indep. NS, since the flow is stationary and ˝𝝏₍t₎⬀v = 0˝).

There are four equations in total (3 components of NS + incompressibility). You 
can solve them for pressure ˝p˝ and velocity ˝⬀v˝ subject to two conditions:
- ˝⬀v = 0˝ on the surface, 
- ˝⬀v˝ is uniform upstream.

## SCALING RELATION -- SIMILAR SOLUTIONS FOR ˝⬀v˝

Given the parameters present in the problem (˝d˝, ˝V˝, ˝ρ˝, ˝ν˝), the only 
dimensionless number you can construct is ˝Re˝. 

Dimensional analysis says the solution for ˝⬀v˝ should obey this form
```
  ÷1V ⬀v = ⬀U(÷1d ⬀x, Re)
```
  - what's going on here? well, in short, we're constructing a dimensionless 
    equation, a SCALING RELATION. remember: your big global here is to solve for 
    ˝⬀v˝, right? you have to solve for ˝⬀v˝ given these specific parameters.  so 
    basically, the solution to ˝⬀v˝ should be a function of some combination of 
    these parameters.
  - ˝÷1V·⬀v˝ is the flow velocity divided by the characteristic velocity, which 
    is dimensionless. 
  - you also know that ˝Re˝ is dimensionless
  - you know that the solution to ˝⬀v˝ can take "wildly" different forms (KT 
    p790) depending on the value of ˝Re˝. laminar flow, emergence of boundary 
    layers, turbulence, etc. the specific equation for ˝⬀v˝ is extremely 
    variable. but you could parametrise it using position ˝⬀x˝ and reynolds 
    number ˝Re˝
  - what?
  - ok, so you know that the equation for ˝⬀v˝ can take very different forms as 
    you change the reynolds number. by "different forms", you really mean the 
    velocity profile looks very different in physical space. so you should be 
    able to express ˝⬀v˝ as a function of position ˝⬀x˝ and reynolds number 
    ˝Re˝.
  - but also, if you make ˝⬀v˝ dimensionless by dividing it by the 
    characteristic velocity, and you also make ˝⬀x˝ dimensionless by dividing it 
    by the characteristic distance, you can get a completely dimless equation. 
    ˝U˝ is a dimensionless function

another little note: if you don't have an incompressible liquid (flow speed is 
fast enough, so sound speed is a relevant parameter), then you can also 
construct a second dimless number -- the mach number -- see above

so anyway, after all that: this is a SCALING RELATION. you can use it to find 
SIMILAR SOLUTIONS.  
- e.g. if you scale ˝V˝ and ˝d˝ by (˝×2˝), then to get a similar flow (same Re 
  number, ˝Re = ÷1ν Vd˝) you need to scale ˝ν˝ by (˝×4˝) to get a similar 
  solution.

## STAGNATION PRESSURE

recall bernoulli's principle: in a low mach number, incompressible, isentropic 
flow, when you increase the speed of a fluid, its static pressure (potential 
energy) will decrease
```
  ÷12 v⁽2⁾ + ∫ ÷1ρ dp = const
```

STAGNATION PRESSURE: the pressure the fluid _would_ have (from B's principle) if 
it came to rest at the front edge of the cylinder, _without any input from 
viscous forces_ (that last caveat is very important). 

Recall that bernoulli's principle assumes there aren't any viscous forces. So 
we're talking about a fluid that goes from moving to standstill, PURELY because 
of the presence of the cylinder (and the pressure it exerts), and not at all 
because of viscosity. To compute this pressure, literally just solve bernoulli's 
equation:
```m
  ½v² + ∫dp·÷1p = const
  ½v² + ÷1ρ·p \bigg|⁽p₍stag₎⁾₍p₍upstream₎₎ = const
```
Let ˝p₍0₎˝ be the upstream pressure, this takes care of the integration 
constant, and you get
```
  p₍stagnation₎ = p₍0₎ + ÷12 ρV⁽2⁾
```

So we're defining this stagnation pressure to be the sole cause of the liquid 
slowing down to zero. ∴ you can say the stagnation pressure acts over the entire 
"front" face of the cylinder. on the back face ("downstream") the pressure is 
just ˝p₍0₎˝.

THEN the net force on the cylinder per unit length is (remember ˝F = P/A˝ so 
˝F/L = ÷1L(P/A)˝)
```mm
  F₍net, unitlength₎
  &= ÷1L ((P₍0₎ + ÷12 ρ V⁽2⁾) A - P₍0₎ A )
  &= ÷1L (½ρV²A)
  &= ½ρV²d
```
  - you can think of the pressure differential acting over a planar segment in 
    the tube (this is why you can use ˝A = dL˝, and you don't calculate it over 
    the curved circular part of the tube)

- this net force is a "rough estimate" of the drag force

## DRAG COEFFICIENT

Ratio of the _actual_ drag force per unit length (the one you really observe) to 
the rough estimate of net force per unit length (as estimated by equating the 
pressure differential using bernoulli's principle):
```
  C₍drag₎ = ⌠F₍drag,ulength₎⧸½ρV²d⌡ ͺͺͺ(2Dͺflow)
```
Force per unit area in case of 3d flows:
```
  C₍drag₎ = ⌠F₍drag,ulength₎⧸½ρV²A⌡ ͺͺͺ(3Dͺflow)
```

The drag coefficient is dimensionless, so it can _only_ depend on the reynolds 
number 
```
  C₍drag₎ = C₍drag₎(Re)
```

The _specific_ functional form of ˝C₍drag₎˝ will depend on the shape and 
orientation of the obstacle.

# FLOW PAST A CYLINDER: INCREASING Re, TRANSITION TO TURBULENCE
--------------------------------------------------------------------------------

## CREEPING FLOW, Re « 1

streamlines are (somewhat) symmetric between upstream and downstream. 

what happens: 
- viscous stresses decelerate the fluid as it moves past the cylinder along the 
  streamlines.
- the pressure is higher on the cylinder's front face rather than back
- both the viscous stresses AND the pressure differential contribute to the net 
  drag force on the cylinder
- momentum removed from the flow is given to the cylinder

near the cylinder, at cylindrical radius r « d/Re: the viscous stresses dominate 
over inertial. fluid momentum is being transferred to the cylinder at rate ∼ 
ρV⁽2⁾

_far from the cylinder surface, at cyl rad r » d/Re:_ the viscous and inertial 
stresses balance. momentum from the fluid isn't being transferred much to the 
cylinder.

this implies that the _"effective cross-sectional width"_ over which the 
cylinder extract's the fluid's momentum is d/Re. note this is much wider than 
the actual cylinder. 
- this should make sense. this is a very slow flow. the flow is highly affected 
  by the cylinder, even at distances far from the cylinder. the flow has little 
  oomph of its own to compete against the cylinder.

thus the net drag force per unit length is F ∼ ρV²d/Re. from the equation for 2d 
drag coefficient, this implies the drag coefficient is ∼ 1/Re at low reynolds 
numbers. experimentally you get C₍drag₎ ∼ 6/Re. 
- again, note this is a huge value. there is a lot of drag imparted by the 
  cylinder.

## INCREASING Re SLIGHTLY, Re ∼ 1

when you increase Re ∼ 1, the effective cross section _reduces_ to roughly the 
same as the cylinder's diameter d, and the drag coefficient decreases to C₍drag₎ 
∼ 1.

now you start seeing the velocity field start to look asymmetric.

## LAMINAR BOUNDARY LAYER FORMATION, Re ∼ 1

At Re ∼ 1, C₍drag₎ ∼ 1, you start seeing a laminar boundary layer emerge. its 
thickness is ˝δ ∼ d / √{Re}˝.

viscous force per unit length due to the boundary layer:
```
  F ∼ ρ V⁽2⁾ d / √{Re}
```
- from KT equations 14.49-14.51 divided by transverse length w, with l ∼ d and 
  v₀ ∼ V

conventional wisdom says that when you increase Re above 1, the drag should 
decrease as ˝C₍drag₎ ∼ 1 / √{Re}˝. if this happened, the boundary layer would 
start to thin and the external flow would start to resemble potential flow (KT 
p793). BUT: this is not so.

## FLOW SEPARATION, RE ∼ 5

what instead happens: at Re ∼ 5, the flow SEPARATES from the back side of the 
cylinder. it's replaced by two "retrograde" eddies. 

why does this separation happen?
- there's an an adverse pressure gradient ˝(⬀v⦁∇)p > 0˝ that starts to develop 
  OUTSIDE the boundary layer, near the cylinder's downstream face
- this causes the separated boundary layer to be replaced by two 
  counter-rotating eddies.

the pressure in the eddies, AND the pressure on the cylinder's downstream face, 
is ∼ p₍0₎ (the fluid's incoming pressure). this pressure is much less than the 
stagnation pressure at the upstream face.

so, anyway, the upshot of all this: the drag coefficient stabilises at ˝C₍drag₎ 
∼ 1˝.

## KARMAN VORTEX STREET, Re ∼ 100

as you increase Re from ∼5 to ∼100, the size of the two eddies inceases.

at Re ∼ 100, the eddies are "shed dynamically". the flow becomes nonstationary.

the eddies are shed alternately in time (first one, then the other). you get 
this lovely little pattern of alternating vortices downstream: karman vortex 
street.

## TURBULENCE, Re ∼ 1000

when Re ∼ 1000, you can't see the downstream vortices anymore.

the wake behind the cylinder has a velocity field that's completely irregular on 
all macroscopic scales. so you say that the downstream flow has become 
turbulent.

## TURBULENT BOUNDARY LAYER, Re ∼ 3⋄10⁵

up till now the boundary layer itself has still been laminar, even if the 
velocity field enclosed within it is turbulent. 

but at Re ∼ 3e5 the boundary layer itself becomes turbulent. this substantially 
reduces the drag coefficient
- explored in KT 15.5

note: when you talk about turbulence onset in the boundary layer, you don't use 
the reynolds number from the cylinder's diameter (which is Re = ÷1ν·Vd). instead 
you have to use the reynolds number using the boundary layer thickness δ ∼ 
d/√{Re} as the characteristic length
```
  Re₍δ₎ = ÷1ν·Vδ ∼ ÷1ν·(Vd·√{Re}) = √{Re}
```
- so what's the reason for this sudden switch in characteristic length scales?

so anyway: the onset of BL turbulence is Re₍δ₎ ∼ √{3e5} ∼ 500. note this is 
roughly the same value as the Re₍d₎ ∼ 1000 for the onset of turbulence in the 
wake.

AH, so basically, turbulence onset always occurs on the order of about ∼1000. 
but turbulence can occur in different places, so you have to use the appropriate 
characteristic length scale.

## 3D NATURE OF TURBULENCE

for Re « 1000 (no turbulence), the flow is translationally symmetric. it's 
independent of distance z down the cylinder (i.e. into/outof the page). so 
you're basically considering a 2d field). also true for the karman vortex 
street.

but the turbulent velocity field when Re ∼> 1000 is 3d. 

so basically, at high Re numbers, small non-translationally-symmetric 
perturbations of the otherwise translationally-symmetric flow can GROW into 
"vigorous" 3d turbulence.

KT: this is a manifestation of the fact that 2d flows can't exhibit all the 
chaotic motion associated with 3d turbulence.
- KT sec 15.4.4


# CRITICAL REYNOLDS NUMBER FOR THE ONSET OF TURBULENCE
--------------------------------------------------------------------------------

there is a critical reynolds number for the onset of turbulence. it is specific 
to each situation. it can be from ∼30 to ∼10⁵, depending on the geometry of the 
flow and the specific lengths and speeds used to define the reynolds number.


# IMPORTANT PROPERTIES OF TURBULENCE
--------------------------------------------------------------------------------

## DISORDER

DISORDER: rich, nonrandom structure. pretty much impossible to reproduce in detail

the disorder is INTRINSIC to the flow.

causes: various instabilities in the flow. you don't need external forces to 
produce it


if you resolve the flow into modes, you find that the phases of the modes 
  aren't completely random: there are strong spatial and temporal correlations

structure: when you look at a freeze-frame of turbulent flow, you often see 
large well defined coherent structures, e.g. eddies and jets. suggests the flow 
is more organised than a pure random superposition of modes
- analagous to how light reflected from the surface of a painting is different 
  from light emitted by a blackbody

INTERMITTENCY: if you monitor one component of velocity over time at a given 
point in the flow, you'll find that strong turbulence irregularly starts and 
ceases. 
- effect is so strong that it's clearly more than a random-mode superposition

## A RANGE OF INTERACTING SCALES

when you fourier analyse the velocity and pressure, you find they vary strongly 
over many "decades" of wave number and frequency. 

these variations are caused by eddies with a large range of sizes. all these 
eddies interact strongly. large eddies feed energy to smaller ones, and so on. 
occasionally the flow of energy also reverses, where small scale turbulent 
structures give rise to large scale ones (causing intermittency)

## VORTICITY

is irregularly distributed in 3d. it varies in magnitude and direction over the 
same wide range of scales as for fluid velocity

## LARGE DISSIPATION

normally turbulent energy is fed from large scales to small in one turnover time 
of a large eddy (v cast). the energy cascades down to smaller and smaller 
lengthscales in shorter and shorter timescales until it reaches eddies that are 
so small that their shear converts turbulent energy to heat.

## EFFICIENT MIXING AND TRANSPORT

anything that can be transported is very efficiently mixed and moved: momentum, 
heat, salt, dyes, etc


# THE ROLE OF VORTICITY
--------------------------------------------------------------------------------

turbulent flows contain TANGLED VORTICITY.

sec 14.2.2: when vorticity is not important, vortex lines are frozen into the 
fluid. they can be stretched by action from neighbouring vortex lines. as a 
bundle of vortex lines is stretched and twisted, the incompressibility of the 
fluid causes the cross section of the bundle to decrease. this causes the 
magnitude of its vorticity to increase. so the length scale on which vorticity 
changes decreases (sec 14.2).

so basically, the continuous lengthening and twisting of fluid elements creates 
vorticity on progressively smaller length scales.

when you're considering 2d flow (the flow has translational symmetry in one 
direction), the vortex lines point in the translational direction. they don't 
get stretched, and there's no driving of turbulent energy to smaller and smaller 
scales. thus is why true turbulence can't occur in 2d. 

BUT there's still a nice phenomenon that isn't quite turbulence, but is similar, 
that does occur in 2d (2d simulations of the kelvin helmholtz instability, KT 
box 15.3).

irl, after the kelvin helmholtz instability is developed, 3d instabilities grow 
strong, and vortex line stretching increases, and the flow develops full 3d 
turbulence. 

this happens in "ostensibly" 2d flows, like the 2d wake behind a cylinder (which 
is not really a 2d problem).
